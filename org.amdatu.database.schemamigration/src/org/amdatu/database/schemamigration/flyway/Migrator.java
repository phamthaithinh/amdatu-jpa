/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.database.schemamigration.flyway;

import java.util.Enumeration;
import java.util.Properties;

import javax.sql.DataSource;

import org.amdatu.database.schemamigration.SchemaMigrationException;
import org.amdatu.database.schemamigration.SchemaMigrationService;
import org.flywaydb.core.Flyway;
import org.osgi.framework.Bundle;
import org.osgi.framework.wiring.BundleWiring;
import org.osgi.service.log.LogService;

public class Migrator implements SchemaMigrationService {
	
	private static final String INIT_ON_MIGRATE = "flyway.initOnMigrate";
	private static final String OUT_OF_ORDER = "flyway.outOfOrder";
	private static final String VALIDATE_ON_MIGRATE = "flyway.validateOnMigrate";
	private static final String TABLE_NAME = "flyway.tableName";
	
	private volatile LogService m_logService;

	
	@Override
	public String migrate(DataSource ds, Bundle bundle, String migrationScriptDir) {
		return migrate(ds, bundle, migrationScriptDir, null);
	}
		
	@Override
	public String migrate(DataSource ds, Bundle bundle, String migrationScriptDir, Properties properties) {
		m_logService.log(LogService.LOG_INFO, "Start Flyway migration of bundle " + bundle.getSymbolicName());
		
		Enumeration<String> entryPaths = bundle.getEntryPaths(migrationScriptDir);
		if(entryPaths == null) {
			throw new SchemaMigrationException("Directory '" + migrationScriptDir + "' doesn't exist");
		}
		
		//TODO: For some reason this failed using javac in a gradle build without the explicit cast...
		ClassLoader bundleClassLoader = ((BundleWiring)bundle.adapt(BundleWiring.class)).getClassLoader();
		return migrate(ds, bundleClassLoader, properties, migrationScriptDir);
	}

	private String migrate(DataSource ds, ClassLoader cl, Properties properties, String... scriptPaths) {	
		boolean initOnMigrate = getOptionalBooleanProperty(properties, INIT_ON_MIGRATE, false);
		boolean validateOnMigrate = getOptionalBooleanProperty(properties, VALIDATE_ON_MIGRATE, true);
		boolean outOfOrder = getOptionalBooleanProperty(properties, OUT_OF_ORDER, false);
		String tableName = getOptionalStringProperty(properties, TABLE_NAME, null);
		try {
			Flyway flyway = new Flyway();
			
			flyway.setClassLoader(cl);
			flyway.setDataSource(ds);
			flyway.setLocations(scriptPaths);
			
			flyway.setInitOnMigrate(initOnMigrate);
			flyway.setOutOfOrder(outOfOrder);
			flyway.setValidateOnMigrate(validateOnMigrate);
			if (tableName != null){
				flyway.setTable(tableName);
			}
			
			flyway.migrate();
			
			return flyway.info().current().getVersion().toString();
		} catch (Exception e) {
			throw new SchemaMigrationException(e);
		}
	}
	
	
	private boolean getOptionalBooleanProperty(Properties properties, String key, boolean defaultValue){
		if (properties == null){
			return defaultValue;
		}
		
		boolean value = defaultValue;
		
		Object property = properties.get(key);
		if (property != null){
			value = Boolean.valueOf((String)property);
		}
		return value;
	}
	
	private String getOptionalStringProperty(Properties properties, String key, String defaultValue){
		if (properties == null){
			return defaultValue;
		}
		
		String value = defaultValue;
		
		Object property = properties.get(key);
		if (property != null){
			value = (String)property;
		}
		return value;
	}
	
}
